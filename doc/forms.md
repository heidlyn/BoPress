# Forms #
与其它Web框架一样，BoPress也提供表单工具，不过采用的是第三方库`WTForms`，只是`WTForms`与`Tornado`之间值传递需要一个辅助类`bopress.forms.FormData`。

	class UserForm(Form):
	    user_login = StringField("UserLogin", validators=[validators.input_required("必须填写")])
	    user_nicename = StringField("UserNiceName")
	    user_email = StringField("UserEmail", validators=[validators.input_required("必须填写"),
	                                                      validators.email("邮箱格式错误")])
	    user_mobile_phone = StringField("UserMobilePhone")
	    display_name = StringField("DisplayName")
	    user_status = IntegerField("UserStatus")

    f = UserForm(FormData(handler))
	if f.validate():
		u = Users()
		f.populate_obj(u)
		s = SessionFactory.session()
		s.add(u)
		s.commit()
	else:
		pass