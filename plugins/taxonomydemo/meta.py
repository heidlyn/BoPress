# -*- coding: utf-8 -*-

from bopress import taxonomy
from bopress.hook import add_static_path, add_submenu_page, add_action

__author__ = 'yezang'


def init():
    taxonomy.create_default_taxonomy("财务部", 'cwb', 'department')
    taxonomy.create_default_taxonomy("业务部", 'ywb', 'department')
    taxonomy.create_default_taxonomy("人事部", 'rsb', 'department')
    taxonomy.create_default_taxonomy("开发部", 'kfb', 'department')


def current_page(current_screen):
    h = current_screen.handler
    h.render("taxonomydemo/tpl/ztree.html",
             current_screen=current_screen)


add_static_path("taxonomydemo/static")
add_submenu_page("dev-example", "Taxonomy Demo", "Taxonomy Demo", "taxonomy-demo", ["r_read"], current_page)
add_action("bo_init", init)
