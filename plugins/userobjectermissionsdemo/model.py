# -*- coding: utf-8 -*-
from sqlalchemy import Column, String, Text, DateTime

from bopress.orm import Entity
from bopress.user import remove_perms_on_delete

__author__ = 'yezang'


@remove_perms_on_delete()
class Posts(Entity):
    post_id = Column(String(22), nullable=False, primary_key=True)
    title = Column(String(255), nullable=False, default="")
    content = Column(Text, nullable=True)
    pubdate = Column(DateTime, nullable=False)
