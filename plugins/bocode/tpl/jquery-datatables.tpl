<script type="text/javascript">
    var {{table_name}}_datatables = null;
    $(document).ready(function () {
        {{table_name}}_datatables = $('#{{table_name}}_list').DataTable({
            "processing": true,
            "ordering": false,
            "serverSide": true,
            "info": true,
            {% if option["freeze_table_columns"]=="Y" %}
            fixedColumns:   {
                leftColumns: 2
            },
            {% end %}
            responsive: true,
            {% if option["fix_table_header"]=="Y" %}
            fixedHeader: {
                header: true,
                footer: true
            },
            {% end %}
            "language": '',
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "ajax": {url: "", type:"POST", "data": function(d){
                // d.custom = $('#myInput').val();
                // etc
            }},
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "columns": [
                {% for col in columns %}
                {% if col.name == primary_key or col.name in exclude_columns %}
                {% continue %}
                {% end %}
                {"data": "{{col.name}}"},
                {% end %}
                {"data": "{{primary_key}}"}
            ],
            "columnDefs": [
                {% set index = 0 %}
                {% for col in columns %}
                {% if col.name == primary_key or col.name in exclude_columns %}
                {% continue %}
                {% end %}
                {
                    //{{col.name}}
                    "render": function (data, type, row) {
                        return data;
                    },
                    {% if col.name in search_columns %}
                    "searchable": true,
                    {% else %}
                    "searchable": false,
                    {% end %}
                    {% if col.name in order_columns %}
                    "orderable": true,
                    {% else %}
                    "orderable": false,
                    {% end %}
                    "visible": true,
                    "targets": {{index}}
                },
                {% set index += 1 %}
                {% end %}
                {
                    //{{primary_key}}
                    "render": function (data, type, row) {
                        var html = "<a href='#"+data+"'>编辑</a>";
                        html += "<a href='#"+data+"' style='margin-left:15px;'>删除</a>";
                        return html;
                    },
                    "visible": true,
                    "targets": -1
                }
            ],
            "order":[
                {% set index1 = 0 %}
                {% for col in columns %}
                {% if col.name in order_columns %}
                [{{index1}}, 'desc'],
                {% end %}
                {% set index1 += 1 %}
                {% end %}
            ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api();
                $(api.column( {{index}} ).footer()).html("");
            }
        });
        {% if option["table_column_search"]=="Y" %}
        {{table_name}}_datatables.columns().every( function () {
            var that = this;
            $( 'input', this.header() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );
        {% end %}
    });
    // {{table_name}}_datatables.ajax.reload(null, false);
</script>
<table id="{{table_name}}_list">
    <thead>
    <tr>
        {% for col in columns %}
        {% if col.name == primary_key or col.name in exclude_columns %}
        {% continue %}
        {% end %}
        {% if col.name in search_columns and option["table_column_search"]=="Y" %}
        <th><input type="text" placeholder="搜索 {{col.name}}" />'</th>
        {% else %}
        <th>{{col.name}}</th>
        {% end %}
        {% end %}
        <th></th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        {% for col in columns %}
        {% if col.name == primary_key or col.name in exclude_columns %}
        {% continue %}
        {% end %}
        {% if (col.name in search_columns) and option["table_column_search"]=="Y" %}
        <th><input type="text" placeholder="搜索 {{col.name}}" />'</th>
        {% else %}
        <th>{{col.name}}</th>
        {% end %}
        {% end %}
        <th></th>
    </tr>
    </tfoot>
</table>